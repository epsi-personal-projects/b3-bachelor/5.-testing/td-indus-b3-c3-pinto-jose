# **TODO IT Now - Plan de test**

## **1° Récapitulatif des fonctionnalités principales du produit**

- Gérer les tâches des employés.
- Gérer l'emploi du temps de ses collaborateurs (mensuel et hebdomadaire) [Manager].
- Timesheet (Le manager peut consulter les timesheets de son équipe **uniquement**) [Manager].
- Emploi du temps 
    - Le manager peut consulter son emploi du temps, mais **uniquement** son N+1 pourra gérer [Manager].
    - Le collaborateur pourra acceder à son emploi du temps et faire également des demandes de modification à son manager [Collaborateur].

## **2° Ressources**

### **a) De quoi a besoin le produit pour fonctionner**

- Des testeurs à fin de faire les tests nécessaires du produit.
- Un environnement de test (serveurs d'intégration, etc.).
- Organisation du testing (Qui testera chaque étape).
- La connaissance des techniques de conception/utilisation des tests.
- Le temps qui va falloir pour tester.
- Des outils de testing (Ils vont permettre d'optimiser / gagner du temps.).

## **3° Détails des tests**

### **a) Tableau**

**[Lien du Tableau](https://docs.google.com/spreadsheets/d/1NoeV0WhS2UzfxPaWyjHkLK1xf66_P51e/edit?usp=sharing&ouid=101861186605697520112&rtpof=true&sd=true)**
